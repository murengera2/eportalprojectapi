"""
Custom User Manager for Nexin projects
"""

from django.contrib.auth.models import UserManager as UManager

class UserManager(UManager):
    """
    Customises Django User Manager, by replacing the required username with phone,
    Creating users require the phone not username.
    """

    def _create_user(self, username, email, password, **extra_fields):
        """
        Creates and saves a User with the given phone_number, email and password.
        """
        if not username:
            raise ValueError('The given phone number must be set')
        email = self.normalize_email(email)
        user = self.model(username=username, email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, email, password, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, email, password, **extra_fields)
