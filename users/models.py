import random, string

from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser, Group
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view

from users.manager import UserManager
from users.utils.fields_utils import NullableCharField
import uuid


class User(AbstractUser):
    """
    This is the Custom user that
    extends Django AbstractUser and adds custom fields.
    """
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    username = NullableCharField(
        max_length=40, unique=True, null=True)  # Overriding Abstract user username
    name = models.CharField(max_length=255,unique=True,blank=True)
    email = NullableCharField(  max_length=100, unique=True, null=True, blank=True)
    phone_number=models.CharField(max_length=15,unique=True,blank=True)

    is_deleted = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'

    objects = UserManager()

    def __str__(self):
        return  str(self.username)

    # def save(self, *args, **kwargs):
    #     if not str(self.phone_number).startswith('+'):
    #         raise Exception("Phone number must start with a '+'")
    #
    #     super(User, self).save(*args, **kwargs)




def generate_password():
    """ Generates a digit based default password """
    list = [random.choice(range(0, 9)) for i in range(0, 10)]
    code = ''.join(str(i) for i in list)
    return code


def _generate_code():
    key = ''.join(random.choices(string.ascii_uppercase + string.digits, k=8))
    return key

class Verification(models.Model):
    code = models.CharField(max_length=12, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    time_created = models.DateTimeField(auto_now=True)
    types = {
        ('activation', 'activation'),
        ('reset', 'reset')
    }
    code_type = models.CharField(max_length=15, default='activation')

    def __str__(self):
        return self.code

    def save(self, *args, **kwargs):
        while not self.code:
            code = _generate_code()
            same_access_code = Verification.objects.filter(code=code)

            if not same_access_code:
                self.code = code

        super(Verification, self).save(*args, **kwargs)


