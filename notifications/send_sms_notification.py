from notifications.africastalking_backend import sms_backend


def send_message(message, user):
    phone_number = user.phone_number

    try:
        sms_backend.send(message, [phone_number])
    except Exception as e:
        print(f'Exception: {e}')
