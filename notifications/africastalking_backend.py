import os
import africastalking


"""

AFRICA'S TALKING SETUP

"""

username = os.environ['AFRICASTALKING_USERNAME']
api_key = os.environ['AFRICASTALKING_APIKEY']

africastalking.initialize(username, api_key)
sms_backend = africastalking.SMS
